<?php
$connection = new PDO('mysql:host=localhost;dbname=khairul', 'root', '');
$page=  isset($_GET['page']) ? (int)$_GET['page']:1;
$per_page=  isset($_GET['per_page']) && $_GET['per_page']<=50 ? (int)$_GET['per_page']:5;

//start
$start=($page>1) ? ($page*$per_page)-$per_page : 0;
//Query

$query="SELECT SQL_CALC_FOUND_ROWS id, name, mobile FROM softdelete LIMIT {$start},{$per_page}";
$stmt=$connection->prepare($query);
$stmt->execute();
 $table=$stmt->fetchAll(PDO::FETCH_ASSOC);
 
  $total=$connection->query("SELECT FOUND_ROWS()as total")->fetch()['total'];
  
   $pages=  ceil($total/$per_page);
   
 
// var_dump($table);
 ?>

<html>
    <head>
        <title>paginations</title>
        <style>
            .Selected{
                font-weight: bold;
                color: green;
                text-decoration: underline;
            }
            a{
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <table>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Mobile</th>
            </tr>
            
                <?php
                $id=1;
                 foreach ($table as $row)
                 { ?>
                 <tr>
                     <td><?php echo $id++ ?></td>
                      <td><?php echo $row['name'] ?></td>
                       <td><?php echo $row['mobile'] ?></td>
                     </tr>

                <?php }
                ?>
                     <tr>
                         <td colspan="5"><?php
                            for($x=1; $x<=$pages; $x++)
                            { ?>
                             <a href="pagination2.php?page=<?php echo $x?>&?per_page=<?php echo $per_page; ?>"
                                 
                                 <?php
                                    if($page===$x){ echo 'class="Selected"'; }
                                 ?>><?php echo $x ?></a> 
                          <?php  }
                         ?></td>
                     </tr>
        </table>
    </body>
</html>