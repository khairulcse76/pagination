<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit1d50d6eaacbedff153aeb84f5ec576a2
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'Apps\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Apps\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit1d50d6eaacbedff153aeb84f5ec576a2::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit1d50d6eaacbedff153aeb84f5ec576a2::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
